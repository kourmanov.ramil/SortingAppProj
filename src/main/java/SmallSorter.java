import java.io.IOException;
import java.util.Scanner;
import java.util.logging.*;

/**
 * Sorter class that does all the work
 */
public class SmallSorter {
    private static final Logger logger = Logger.getLogger(SmallSorter.class.getName());
    private static FileHandler fileHandler;;
    {
        try {
            fileHandler = new FileHandler("E:/Java_projects/Introduction_to_Maven_and_JUnit/SortingApp/src/main/logs/ExceptionsLog.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    int numberOfArguments = 0;
    int[] argumentsArray = new int[0];

    public SmallSorter(){}

    public SmallSorter(int num){
        numberOfArguments = num;
    }

    /**
     * Finds out number of arguments which will be entered later
     * @param argNumber - string containing the number
     */
    public void setNumberOfArguments(String argNumber){
        logger.setUseParentHandlers(false);
        logger.addHandler(fileHandler);
        logger.setLevel(Level.WARNING);

        try {
            numberOfArguments = Integer.parseInt(argNumber);
        } catch (Exception exception) {
            logger.warning("NumberFormatException thrown in setNumberOfArguments(String)");
            throw new NumberFormatException("Number of arguments must be an integer number.");
        }

        if (numberOfArguments < 1 || numberOfArguments > 10) {
            logger.warning("IllegalArgumentException thrown in setNumberOfArguments(String)");
            throw new IllegalArgumentException("Number of arguments must be from 1 to 10");
        }
    }

    /**
     * Returns number of arguments
     */
    public int getNumberOfArguments() {
        return numberOfArguments;
    }

    /**
     * Finds out the arguments themselves
     * @param argArray - string containing arguments separated by spaces
     */
    public void setArgumentsArray(String argArray) {
        logger.setUseParentHandlers(false);
        logger.addHandler(fileHandler);
        logger.setLevel(Level.WARNING);

        int argsNumber = getNumberOfArguments();
        argumentsArray = new int[argsNumber];
        Scanner input = new Scanner(argArray);
        try {
            for (int i = 0; i < argsNumber; i++) {
                argumentsArray[i] = input.nextInt();
            }
        } catch (Exception exception) {
            logger.warning("NumberFormatException thrown in setArgumentsArray(String)");
            throw new NumberFormatException("Arguments must be " + argsNumber + " integer numbers");
        }
        for (int i = 0; i < argsNumber; i++) {
            for (int j = i + 1; j < argsNumber; j++) {
                if (argumentsArray[j] < argumentsArray[i]) {
                    int temp = argumentsArray[j];
                    argumentsArray[j] = argumentsArray[i];
                    argumentsArray[i] = temp;
                }
            }
        }
    }

    /**
     * Returns string containing arguments sorted in ascending order separated by spaces
     */
    public String getArgumentsArray() {
        String argumentsSorted = "";
        for (int i = 0; i < getNumberOfArguments(); i++) {
            argumentsSorted += argumentsArray[i] + " " ;
        }
        return argumentsSorted;
    }
}
