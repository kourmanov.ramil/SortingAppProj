import java.io.IOException;
import java.util.Scanner;
import java.util.logging.*;

public class SortingApp {
    private static final Logger logger = Logger.getLogger(SortingApp.class.getName());
    private static FileHandler fh;

    static {
        try {
            fh = new FileHandler("E:/Java_projects/Introduction_to_Maven_and_JUnit/SortingApp/src/main/logs/WorkingLog.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Main class that starts the application
     * @param args unused
     */
    public static void main(String[] args){
        logger.setUseParentHandlers(false);
        logger.addHandler(fh);
        logger.setLevel(Level.INFO);

        Scanner input = new Scanner(System.in);

        System.out.println("Choose number of arguments (1-10): ");
        String argNumber = input.nextLine();
        logger.info("Number of arguments: " + argNumber + " entered");

        SmallSorter sorter = new SmallSorter();
        sorter.setNumberOfArguments(argNumber);

        System.out.println("Enter " + sorter.getNumberOfArguments() + " arguments separated by spaces: ");
        String argArray = input.nextLine();
        logger.info("Arguments: " + argArray + " entered");

        sorter.setArgumentsArray(argArray);
        System.out.println(sorter.getArgumentsArray());
    }
}
