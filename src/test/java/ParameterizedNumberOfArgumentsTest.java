import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;

import java.util.Arrays;
import java.util.Collection;

/**
 * Check if argument number set module detects data entered in wrong format
 */
@RunWith(Parameterized.class)
public class ParameterizedNumberOfArgumentsTest {
    @Parameter
    public String numberOfArguments;

    @Parameters
    public static Collection data() {
        return Arrays.asList(new String[]{"s", "str", "\n", " ", "1 2", "2.1", "2147483648", "-2147483649"});
    }

    @Test(expected = NumberFormatException.class)
    public void testPositiveIntOverLimitNumberOfArguments() {
        SmallSorter sorter = new SmallSorter();
        sorter.setNumberOfArguments(numberOfArguments);
    }
}
