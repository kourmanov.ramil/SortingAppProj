import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Check if argument sorting module works correctly with proper data entered
 */
public class ArgumentsArrayTest {

    @Test
    public void testPositiveIntLimitArgument() {
        SmallSorter sorter = new SmallSorter(1);
        sorter.setArgumentsArray("2147483647");
        assertEquals("2147483647 ", sorter.getArgumentsArray());
    }

    @Test
    public void testNegativeIntLimitArgument() {
        SmallSorter sorter = new SmallSorter(1);
        sorter.setArgumentsArray("-2147483648");
        assertEquals("-2147483648 ", sorter.getArgumentsArray());
    }

    @Test
    public void testPositiveIntLimitArguments() {
        SmallSorter sorter = new SmallSorter(3);
        sorter.setArgumentsArray("4 2147483647 -5");
        assertEquals("-5 4 2147483647 ", sorter.getArgumentsArray());
    }

    @Test
    public void testNegativeIntLimitArguments() {
        SmallSorter sorter = new SmallSorter(3);
        sorter.setArgumentsArray("9 -2147483648 -15");
        assertEquals("-2147483648 -15 9 ", sorter.getArgumentsArray());
    }

    @Test
    public void testAllowedArguments() {
        SmallSorter sorter = new SmallSorter(7);
        sorter.setArgumentsArray("10 15 -4 -8 1 6 22");
        assertEquals("-8 -4 1 6 10 15 22 ", sorter.getArgumentsArray());
    }
//    @Test(expected = NumberFormatException.class)
//    public void testMoreArguments() {
//        SmallSorter sorter = new SmallSorter(5);
//        sorter.setArgumentsArray("2 4 8 16 5 7 9 3 2 1");
//    }

}
