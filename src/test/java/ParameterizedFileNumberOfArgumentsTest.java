import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Check if argument number set module detects data outside allowed range
 */
@RunWith(Parameterized.class)
public class ParameterizedFileNumberOfArgumentsTest {
    @Parameter
    public String numberOfArguments;

    @Parameters
    public static Collection data() throws IOException {
        File newFile = new File("E:/Java_projects/Introduction_to_Maven_and_JUnit/SortingApp/src/test/resources/IllegalArgumentsNumber");
        List<String> paramList = FileUtils.readLines(newFile, (Charset) null);
        return paramList;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLowerOverLimitNumberOfArguments() {
        SmallSorter sorter = new SmallSorter();
        sorter.setNumberOfArguments(numberOfArguments);
    }
}
