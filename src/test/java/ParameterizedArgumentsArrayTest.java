import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;

import java.util.Arrays;
import java.util.Collection;

/**
 * Check if argument sorting module detects data entered in wrong format
 */
@RunWith(Parameterized.class)
public class ParameterizedArgumentsArrayTest {
    @Parameter(0)
    public int numberOfArguments;
    @Parameter(1)
    public String argArray;

    @Parameters
    public static Collection data() {
        return Arrays.asList(new Object[][]{{1,"s"}, {1,"str"}, {1,"\n"}, {1," "}, {1,"2147483648"}, {1,"2147483649"},
                {4,"1 s 5 e"}, {4,"2 \n 4 \n"}, {5,"2 4"}, {4,"1 5 2147483648 2"}, {4,"17 3 -2147483649 -2"},});
    }

    @Test(expected = NumberFormatException.class)
    public void testNegativeIntOverLimitArguments() {
        SmallSorter sorter = new SmallSorter(numberOfArguments);
        sorter.setArgumentsArray(argArray);
    }
}
