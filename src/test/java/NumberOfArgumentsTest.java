import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Check if argument number set module works correctly with proper data entered
 */
public class NumberOfArgumentsTest {

    @Test(expected = IllegalArgumentException.class)
    public void testPositiveIntLimitNumberOfArguments() {
        SmallSorter sorter = new SmallSorter();
        sorter.setNumberOfArguments("2147483647");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeIntLimitNumberOfArguments() {
        SmallSorter sorter = new SmallSorter();
        sorter.setNumberOfArguments("-2147483648");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeIntNumberOfArguments() {
        SmallSorter sorter = new SmallSorter();
        sorter.setNumberOfArguments("-1");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpperOverLimitNumberOfArguments() {
        SmallSorter sorter = new SmallSorter();
        sorter.setNumberOfArguments("11");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLowerOverLimitNumberOfArguments() {
        SmallSorter sorter = new SmallSorter();
        sorter.setNumberOfArguments("0");
    }

    @Test()
    public void testAllowedNumberOfArguments1() {
        SmallSorter sorter = new SmallSorter();
        sorter.setNumberOfArguments("1"); //param
        assertEquals(1, sorter.getNumberOfArguments());
    }

    @Test()
    public void testAllowedNumberOfArguments2() {
        SmallSorter sorter = new SmallSorter();
        sorter.setNumberOfArguments("5"); //param
        assertEquals(5, sorter.getNumberOfArguments());
    }

    @Test()
    public void testAllowedNumberOfArguments3() {
        SmallSorter sorter = new SmallSorter();
        sorter.setNumberOfArguments("10"); //param
        assertEquals(10, sorter.getNumberOfArguments());
    }
}
